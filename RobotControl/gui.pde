/* =========================================================
 * ====                   WARNING                        ===
 * =========================================================
 * The code in this tab has been generated from the GUI form
 * designer and care should be taken when editing this file.
 * Only add/edit code inside the event handlers i.e. only
 * use lines between the matching comment tags. e.g.

 void myBtnEvents(GButton button) { //_CODE_:button1:12356:
     // It is safe to enter your event code here  
 } //_CODE_:button1:12356:
 
 * Do not rename this tab!
 * =========================================================
 */

public void button1_click1(GButton source, GEvent event) { //_CODE_:hablar:491608:
  if (event==GEvent.CLICKED) {
    EnviaraBD('V', 0, textfield1.getText());
    EnviarOSC("V", 0, textfield1.getText());
  }
} //_CODE_:hablar:491608:

public void textfield1_change1(GTextField source, GEvent event) { //_CODE_:textfield1:997622:
  if (event==GEvent.GETS_FOCUS) {
    teclado=false;
  }
  if (event==GEvent.LOST_FOCUS) {
    teclado=true;
  }
} //_CODE_:textfield1:997622:

public void button2_click1(GButton source, GEvent event) { //_CODE_:abrir_ojo_iz:392861:
  if (event==GEvent.CLICKED) {
    EnviaraBD('I', 0, "");
    EnviarOSC("I", 0, "");
  }
} //_CODE_:abrir_ojo_iz:392861:

public void button3_click1(GButton source, GEvent event) { //_CODE_:abrir_ojo_de:982429:
  if (event==GEvent.CLICKED) {
    EnviaraBD('D', 0, "");
    EnviarOSC("D", 0, "");
  }
} //_CODE_:abrir_ojo_de:982429:

public void button4_click1(GButton source, GEvent event) { //_CODE_:cerrar_ojo_iz:554491:
  if (event==GEvent.CLICKED) {
    EnviaraBD('i', 0, "");
    EnviarOSC("i", 0, "");
  }
} //_CODE_:cerrar_ojo_iz:554491:

public void button5_click1(GButton source, GEvent event) { //_CODE_:cerrar_ojo_de:935670:
  if (event==GEvent.CLICKED) {
    EnviaraBD('d', 0, "");
    EnviarOSC("d", 0, "");
  }
} //_CODE_:cerrar_ojo_de:935670:

public void button6_click1(GButton source, GEvent event) { //_CODE_:borrar_historial:367717:
  if (event==GEvent.CLICKED) {
    msql.query("TRUNCATE TABLE instrucciones");
  }
} //_CODE_:borrar_historial:367717:

public void button7_click1(GButton source, GEvent event) { //_CODE_:modo_debug:217696:
  if (event==GEvent.CLICKED) {
    EnviarOSC("B", 0, "");
    EnviaraBD('B', 0, "");
  }
} //_CODE_:modo_debug:217696:

public void button8_click1(GButton source, GEvent event) { //_CODE_:luz_trasera:891022:
  if (event==GEvent.CLICKED) {
    luztrasera=!luztrasera;
    if (luztrasera) {
      myPort.write('U');
    } else {
      myPort.write('u');
    }
  }
} //_CODE_:luz_trasera:891022:

public void button9_click1(GButton source, GEvent event) { //_CODE_:luz_delantera:906130:
  if (event==GEvent.CLICKED) {
    luzdelantera=!luzdelantera;
    if (luzdelantera) {
      myPort.write('W');
    } else {
      myPort.write('w');
    }
  }
} //_CODE_:luz_delantera:906130:

public void button10_click1(GButton source, GEvent event) { //_CODE_:bocina:473581:
  if (event==GEvent.CLICKED) {
    estadobocina=!estadobocina;
    if (estadobocina) {
      myPort.write('V');
    } else {
      myPort.write('v');
    }
  }
} //_CODE_:bocina:473581:

public void button1_click2(GButton source, GEvent event) { //_CODE_:seguir_cara:815871:
  if (event==GEvent.CLICKED) {
    seguircara=!seguircara;
    if (seguircara) {
      seguir_cara.setLocalColorScheme(3);
    } else {
      seguir_cara.setLocalColorScheme(6);
    }
  }
} //_CODE_:seguir_cara:815871:

public void button1_click3(GButton source, GEvent event) { //_CODE_:button1:435751:
  if (event==GEvent.CLICKED) {
    EnviarOSC("L", 0, "");
    EnviaraBD('L', 0, "");
  }
} //_CODE_:button1:435751:

public void button2_click2(GButton source, GEvent event) { //_CODE_:button2:611648:
  if (event==GEvent.CLICKED) {
    EnviarOSC("l", 0, "");
    EnviaraBD('l', 0, "");
  }
} //_CODE_:button2:611648:

public void slider1_change1(GSlider source, GEvent event) { //_CODE_:gradoscuello:214577:
  switch(source.getValueI()) {
  case 5:
    myPort.write('q');
    break;
  case 4:
    myPort.write('8');
    break;
  case 3:
    myPort.write('6');
    break;
  case 2:
    myPort.write('4');
    break;
  case 1:
    myPort.write('1');
    break;
  }
} //_CODE_:gradoscuello:214577:

public void button3_click2Cuel(GButton source, GEvent event) { //_CODE_:CuelloIzq:971050:
  if (event==GEvent.CLICKED) {
    myPort.write('f');
    myPort.write(' ');
  }
} //_CODE_:CuelloIzq:971050:

public void button3_click2(GButton source, GEvent event) { //_CODE_:CuelloDer:993011:
  if (event==GEvent.CLICKED) {
    myPort.write('b');
    myPort.write(' ');
  }
} //_CODE_:CuelloDer:993011:

public void button3_click3(GButton source, GEvent event) { //_CODE_:camara1:218401:
  if (event==GEvent.CLICKED) {
    estcam1=!estcam1;
  }
} //_CODE_:camara1:218401:

public void button3_click4(GButton source, GEvent event) { //_CODE_:camara2:994131:
  if (event==GEvent.CLICKED) {
    estcam2=!estcam2;
  }
} //_CODE_:camara2:994131:

public void knob1_turn1(GKnob source, GEvent event) { //_CODE_:dirojoder:308736:
  EnviarOSC("M", dirojoder.getValueI(), "");
  if (ojoscincro.isSelected()) {
    EnviarOSC("N", dirojoder.getValueI(), "");
    dirojoizq.setValue(dirojoder.getValueI());
  }
  if (event==GEvent.LOST_FOCUS) {
    EnviaraBD('m', dirojoder.getValueI(), "");
  }
} //_CODE_:dirojoder:308736:

public void knob1_turn2(GKnob source, GEvent event) { //_CODE_:dirojoizq:355217:
  EnviarOSC("N", dirojoizq.getValueI(), "");
  if (event==GEvent.LOST_FOCUS) {
    EnviaraBD('M', dirojoizq.getValueI(), "");
  }
} //_CODE_:dirojoizq:355217:

public void button3_click5(GButton source, GEvent event) { //_CODE_:enviarder:243936:
  if (event==GEvent.CLICKED) {
    EnviaraBD('m', dirojoder.getValueI(), "");
  }
} //_CODE_:enviarder:243936:

public void button3_click6(GButton source, GEvent event) { //_CODE_:enviarizq:997221:
  if (event==GEvent.CLICKED) {
    EnviaraBD('M', dirojoizq.getValueI(), "");
  }
} //_CODE_:enviarizq:997221:

public void button3_click7(GButton source, GEvent event) { //_CODE_:mhistorial:319165:
  if (event==GEvent.CLICKED) {
    MostrarHistorialBD();
  }
} //_CODE_:mhistorial:319165:

public void slider1_change2(GSlider source, GEvent event) { //_CODE_:distojoder:547483:
  EnviarOSC("m", distojoder.getValueI(), "");
  if (ojoscincro.isSelected()) {
    EnviarOSC("n", distojoder.getValueI(), "");
    distojoizq.setValue(distojoder.getValueI());
  }
} //_CODE_:distojoder:547483:

public void slider1_change3(GSlider source, GEvent event) { //_CODE_:distojoizq:364042:
  EnviarOSC("n", distojoizq.getValueI(), "");
} //_CODE_:distojoizq:364042:

public void button3_click8(GButton source, GEvent event) { //_CODE_:selcamara:955265:
  if (event==GEvent.CLICKED) {
    EnviarOSC("X", 0, "");
  }
} //_CODE_:selcamara:955265:

public void button3_click9(GButton source, GEvent event) { //_CODE_:actsensores:218203:
  if (event==GEvent.CLICKED) {
    EnviarOSC("A", 0, "");
  }
} //_CODE_:actsensores:218203:

public void button3_click10(GButton source, GEvent event) { //_CODE_:dactsensores:738890:
  if (event==GEvent.CLICKED) {
    EnviarOSC("a", 0, "");
  }
} //_CODE_:dactsensores:738890:

public void checkbox1_clicked1(GCheckbox source, GEvent event) { //_CODE_:ojoscincro:555695:
  println("ojoscincro - GCheckbox >> GEvent." + event + " @ " + millis());
} //_CODE_:ojoscincro:555695:

public void button3_click11(GButton source, GEvent event) { //_CODE_:s1a0:213307:
  println("s1a0 - GButton >> GEvent." + event + " @ " + millis());
  myPort.write('a');
} //_CODE_:s1a0:213307:

public void button3_click12(GButton source, GEvent event) { //_CODE_:s1a45:601160:
  println("s1a45 - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('c');
} //_CODE_:s1a45:601160:

public void button3_click13(GButton source, GEvent event) { //_CODE_:s1a90:537028:
  println("s1a90 - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('e');
} //_CODE_:s1a90:537028:

public void button3_click14(GButton source, GEvent event) { //_CODE_:s1a135:351520:
  println("s1a135 - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('p');
} //_CODE_:s1a135:351520:

public void button3_click15(GButton source, GEvent event) { //_CODE_:s1a180:404739:
  println("s1a180 - GButton >> GEvent." + event + " @ " + millis());
  myPort.write('z');
} //_CODE_:s1a180:404739:

public void button3_click16(GButton source, GEvent event) { //_CODE_:s1m1p:852449:
  println("s1m1p - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('t');
      myPort.write(' ');
} //_CODE_:s1m1p:852449:

public void button3_click17(GButton source, GEvent event) { //_CODE_:s1m10p:545614:
  println("s1m10p - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('y');
      myPort.write(' ');
} //_CODE_:s1m10p:545614:

public void button3_click18(GButton source, GEvent event) { //_CODE_:s1m45p:571639:
  println("s1m45p - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('k');
      myPort.write(' ');
} //_CODE_:s1m45p:571639:

public void button3_click19(GButton source, GEvent event) { //_CODE_:s1m1n:586452:
  println("s1m1n - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('m');
      myPort.write(' ');
} //_CODE_:s1m1n:586452:

public void button3_click20(GButton source, GEvent event) { //_CODE_:s1m10n:281591:
  println("s1m10n - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('n');
      myPort.write(' ');
} //_CODE_:s1m10n:281591:

public void button3_click21(GButton source, GEvent event) { //_CODE_:s1m45n:283711:
  println("s1m45n - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('o');
      myPort.write(' ');
} //_CODE_:s1m45n:283711:

public void button3_click22(GButton source, GEvent event) { //_CODE_:s2a0:556272:
  println("s2a0 - GButton >> GEvent." + event + " @ " + millis());
  myPort.write('A');
} //_CODE_:s2a0:556272:

public void button3_click23(GButton source, GEvent event) { //_CODE_:s2a45:943729:
  println("s2a45 - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('C');
} //_CODE_:s2a45:943729:

public void button3_click24(GButton source, GEvent event) { //_CODE_:s2a90:795588:
  println("s2a90 - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('E');
} //_CODE_:s2a90:795588:

public void button3_click25(GButton source, GEvent event) { //_CODE_:s2a135:521183:
  println("s2a135 - GButton >> GEvent." + event + " @ " + millis());
  myPort.write('P');
} //_CODE_:s2a135:521183:

public void button3_click26(GButton source, GEvent event) { //_CODE_:s2a180:718545:
  println("s2a180 - GButton >> GEvent." + event + " @ " + millis());
  myPort.write('Z');
} //_CODE_:s2a180:718545:

public void button3_click27(GButton source, GEvent event) { //_CODE_:s2m1n:400462:
  println("s2m1n - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('M');
      myPort.write(' ');
} //_CODE_:s2m1n:400462:

public void button3_click28(GButton source, GEvent event) { //_CODE_:s2m10n:504720:
  println("s2m10n - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('N');
      myPort.write(' ');
} //_CODE_:s2m10n:504720:

public void button3_click29(GButton source, GEvent event) { //_CODE_:s2m45n:264987:
  println("s2m45n - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('O');
      myPort.write(' ');
} //_CODE_:s2m45n:264987:

public void button3_click30(GButton source, GEvent event) { //_CODE_:s2m1p:704511:
  println("s2m1p - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('T');
      myPort.write(' ');
} //_CODE_:s2m1p:704511:

public void button3_click31(GButton source, GEvent event) { //_CODE_:s2m10p:331048:
  println("s2m10p - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('Y');
      myPort.write(' ');
} //_CODE_:s2m10p:331048:

public void button3_click32(GButton source, GEvent event) { //_CODE_:s2m45p:911919:
  println("s2m45p - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('K');
      myPort.write(' ');
} //_CODE_:s2m45p:911919:

public void button3_click33(GButton source, GEvent event) { //_CODE_:abrepinza:688729:
  println("abrepinza - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('r');
      myPort.write(' ');  
} //_CODE_:abrepinza:688729:

public void button3_click34(GButton source, GEvent event) { //_CODE_:cerrarpinza:607230:
  println("cerrarpinza - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('l');
      myPort.write(' ');
} //_CODE_:cerrarpinza:607230:

public void button3_click35(GButton source, GEvent event) { //_CODE_:liberar:747931:
  println("liberapinza - GButton >> GEvent." + event + " @ " + millis());
      myPort.write('D');
} //_CODE_:liberar:747931:



// Create all the GUI controls. 
// autogenerated do not edit
public void createGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setMouseOverEnabled(false);
  surface.setTitle("Sketch Window");
  hablar = new GButton(this, 10, 820, 80, 30);
  hablar.setText("Decir texto");
  hablar.addEventHandler(this, "button1_click1");
  textfield1 = new GTextField(this, 100, 820, 490, 30, G4P.SCROLLBARS_NONE);
  textfield1.setOpaque(true);
  textfield1.addEventHandler(this, "textfield1_change1");
  abrir_ojo_iz = new GButton(this, 510, 730, 80, 30);
  abrir_ojo_iz.setText("Abrir ojo IZQ.");
  abrir_ojo_iz.addEventHandler(this, "button2_click1");
  abrir_ojo_de = new GButton(this, 10, 730, 80, 30);
  abrir_ojo_de.setText("Abrir ojo DER.");
  abrir_ojo_de.addEventHandler(this, "button3_click1");
  cerrar_ojo_iz = new GButton(this, 510, 770, 80, 30);
  cerrar_ojo_iz.setText("Cerrar ojo IZQ.");
  cerrar_ojo_iz.addEventHandler(this, "button4_click1");
  cerrar_ojo_de = new GButton(this, 10, 770, 80, 30);
  cerrar_ojo_de.setText("Cerrar ojo DER.");
  cerrar_ojo_de.addEventHandler(this, "button5_click1");
  borrar_historial = new GButton(this, 1700, 780, 80, 30);
  borrar_historial.setText("BORRAR HISTORIAL");
  borrar_historial.setLocalColorScheme(GCScheme.RED_SCHEME);
  borrar_historial.addEventHandler(this, "button6_click1");
  modo_debug = new GButton(this, 1700, 820, 80, 30);
  modo_debug.setText("Modo DEBUG");
  modo_debug.addEventHandler(this, "button7_click1");
  luz_trasera = new GButton(this, 810, 820, 80, 30);
  luz_trasera.setText("Luz trasera");
  luz_trasera.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  luz_trasera.addEventHandler(this, "button8_click1");
  luz_delantera = new GButton(this, 810, 780, 80, 30);
  luz_delantera.setText("Luz delantera");
  luz_delantera.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  luz_delantera.addEventHandler(this, "button9_click1");
  bocina = new GButton(this, 810, 740, 80, 30);
  bocina.setText("Bocina");
  bocina.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  bocina.addEventHandler(this, "button10_click1");
  seguir_cara = new GButton(this, 1510, 780, 80, 30);
  seguir_cara.setText("Seguir cara");
  seguir_cara.addEventHandler(this, "button1_click2");
  button1 = new GButton(this, 620, 740, 80, 30);
  button1.setText("Encender linterna");
  button1.addEventHandler(this, "button1_click3");
  button2 = new GButton(this, 620, 780, 80, 30);
  button2.setText("Apagar linterna");
  button2.addEventHandler(this, "button2_click2");
  gradoscuello = new GSlider(this, 900, 740, 170, 40, 10.0);
  gradoscuello.setLimits(1, 1, 5);
  gradoscuello.setNbrTicks(5);
  gradoscuello.setStickToTicks(true);
  gradoscuello.setNumberFormat(G4P.INTEGER, 0);
  gradoscuello.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  gradoscuello.setOpaque(false);
  gradoscuello.addEventHandler(this, "slider1_change1");
  CuelloIzq = new GButton(this, 900, 790, 60, 20);
  CuelloIzq.setText("Izquierda");
  CuelloIzq.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  CuelloIzq.addEventHandler(this, "button3_click2Cuel");
  CuelloDer = new GButton(this, 1010, 790, 60, 20);
  CuelloDer.setText("Derecha");
  CuelloDer.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  CuelloDer.addEventHandler(this, "button3_click2");
  camara1 = new GButton(this, 1600, 780, 80, 30);
  camara1.setText("Cámara 01");
  camara1.addEventHandler(this, "button3_click3");
  camara2 = new GButton(this, 1600, 820, 80, 30);
  camara2.setText("Cámara 02");
  camara2.addEventHandler(this, "button3_click4");
  dirojoder = new GKnob(this, 110, 750, 60, 60, 0.8);
  dirojoder.setTurnRange(0, 360);
  dirojoder.setTurnMode(GKnob.CTRL_ANGULAR);
  dirojoder.setShowArcOnly(false);
  dirojoder.setOverArcOnly(true);
  dirojoder.setIncludeOverBezel(false);
  dirojoder.setShowTrack(true);
  dirojoder.setLimits(0.0, 0.0, 360.0);
  dirojoder.setNbrTicks(10);
  dirojoder.setStickToTicks(true);
  dirojoder.setShowTicks(true);
  dirojoder.setEasing(10.0);
  dirojoder.setOpaque(false);
  dirojoder.addEventHandler(this, "knob1_turn1");
  dirojoizq = new GKnob(this, 430, 750, 60, 60, 0.8);
  dirojoizq.setTurnRange(0, 360);
  dirojoizq.setTurnMode(GKnob.CTRL_ANGULAR);
  dirojoizq.setShowArcOnly(false);
  dirojoizq.setOverArcOnly(true);
  dirojoizq.setIncludeOverBezel(false);
  dirojoizq.setShowTrack(true);
  dirojoizq.setLimits(0.0, 0.0, 360.0);
  dirojoizq.setNbrTicks(10);
  dirojoizq.setStickToTicks(true);
  dirojoizq.setShowTicks(true);
  dirojoizq.setEasing(10.0);
  dirojoizq.setOpaque(false);
  dirojoizq.addEventHandler(this, "knob1_turn2");
  enviarder = new GButton(this, 110, 720, 60, 30);
  enviarder.setText("Enviar");
  enviarder.addEventHandler(this, "button3_click5");
  enviarizq = new GButton(this, 430, 720, 60, 30);
  enviarizq.setText("Enviar");
  enviarizq.addEventHandler(this, "button3_click6");
  mhistorial = new GButton(this, 1700, 740, 80, 30);
  mhistorial.setText("Mostrar historial");
  mhistorial.addEventHandler(this, "button3_click7");
  distojoder = new GSlider(this, 220, 720, 90, 40, 10.0);
  distojoder.setRotation(PI/2, GControlMode.CORNER);
  distojoder.setLimits(0, 0, 100);
  distojoder.setNbrTicks(101);
  distojoder.setNumberFormat(G4P.INTEGER, 0);
  distojoder.setOpaque(false);
  distojoder.addEventHandler(this, "slider1_change2");
  distojoizq = new GSlider(this, 420, 720, 90, 40, 10.0);
  distojoizq.setRotation(PI/2, GControlMode.CORNER);
  distojoizq.setLimits(0, 0, 100);
  distojoizq.setNbrTicks(101);
  distojoizq.setNumberFormat(G4P.INTEGER, 0);
  distojoizq.setOpaque(false);
  distojoizq.addEventHandler(this, "slider1_change3");
  selcamara = new GButton(this, 620, 820, 80, 30);
  selcamara.setText("Cambia cámara");
  selcamara.addEventHandler(this, "button3_click8");
  actsensores = new GButton(this, 710, 740, 80, 30);
  actsensores.setText("Activar sensores");
  actsensores.addEventHandler(this, "button3_click9");
  dactsensores = new GButton(this, 710, 780, 80, 30);
  dactsensores.setText("Desactivar sensores");
  dactsensores.addEventHandler(this, "button3_click10");
  ojoscincro = new GCheckbox(this, 238, 786, 120, 20);
  ojoscincro.setIconAlign(GAlign.LEFT, GAlign.MIDDLE);
  ojoscincro.setText("Sincronizar ojos");
  ojoscincro.setOpaque(false);
  ojoscincro.addEventHandler(this, "checkbox1_clicked1");
  s1a0 = new GButton(this, 1390, 830, 50, 20);
  s1a0.setText("0 g");
  s1a0.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1a0.addEventHandler(this, "button3_click11");
  s1a45 = new GButton(this, 1390, 800, 50, 20);
  s1a45.setText("45 g");
  s1a45.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1a45.addEventHandler(this, "button3_click12");
  s1a90 = new GButton(this, 1390, 770, 50, 20);
  s1a90.setText("90 g");
  s1a90.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1a90.addEventHandler(this, "button3_click13");
  s1a135 = new GButton(this, 1390, 740, 50, 20);
  s1a135.setText("135 g");
  s1a135.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1a135.addEventHandler(this, "button3_click14");
  s1a180 = new GButton(this, 1390, 710, 50, 20);
  s1a180.setText("180 g");
  s1a180.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1a180.addEventHandler(this, "button3_click15");
  s1m1p = new GButton(this, 1450, 800, 40, 20);
  s1m1p.setText("+1");
  s1m1p.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1m1p.addEventHandler(this, "button3_click16");
  s1m10p = new GButton(this, 1450, 770, 40, 20);
  s1m10p.setText("+10");
  s1m10p.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1m10p.addEventHandler(this, "button3_click17");
  s1m45p = new GButton(this, 1450, 740, 40, 20);
  s1m45p.setText("+45");
  s1m45p.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1m45p.addEventHandler(this, "button3_click18");
  s1m1n = new GButton(this, 1340, 800, 40, 20);
  s1m1n.setText("-1");
  s1m1n.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1m1n.addEventHandler(this, "button3_click19");
  s1m10n = new GButton(this, 1340, 770, 40, 20);
  s1m10n.setText("-10");
  s1m10n.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1m10n.addEventHandler(this, "button3_click20");
  s1m45n = new GButton(this, 1340, 740, 40, 20);
  s1m45n.setText("-45");
  s1m45n.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  s1m45n.addEventHandler(this, "button3_click21");
  s2a0 = new GButton(this, 1230, 830, 50, 20);
  s2a0.setText("0 g");
  s2a0.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2a0.addEventHandler(this, "button3_click22");
  s2a45 = new GButton(this, 1230, 800, 50, 20);
  s2a45.setText("45 g");
  s2a45.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2a45.addEventHandler(this, "button3_click23");
  s2a90 = new GButton(this, 1230, 770, 50, 20);
  s2a90.setText("90 g");
  s2a90.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2a90.addEventHandler(this, "button3_click24");
  s2a135 = new GButton(this, 1230, 740, 50, 20);
  s2a135.setText("135 g");
  s2a135.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2a135.addEventHandler(this, "button3_click25");
  s2a180 = new GButton(this, 1230, 710, 50, 20);
  s2a180.setText("180 g");
  s2a180.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2a180.addEventHandler(this, "button3_click26");
  s2m1n = new GButton(this, 1180, 800, 40, 20);
  s2m1n.setText("-1");
  s2m1n.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2m1n.addEventHandler(this, "button3_click27");
  s2m10n = new GButton(this, 1180, 770, 40, 20);
  s2m10n.setText("-10");
  s2m10n.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2m10n.addEventHandler(this, "button3_click28");
  s2m45n = new GButton(this, 1180, 740, 40, 20);
  s2m45n.setText("-45");
  s2m45n.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2m45n.addEventHandler(this, "button3_click29");
  s2m1p = new GButton(this, 1290, 800, 40, 20);
  s2m1p.setText("+1");
  s2m1p.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2m1p.addEventHandler(this, "button3_click30");
  s2m10p = new GButton(this, 1290, 770, 40, 20);
  s2m10p.setText("+10");
  s2m10p.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2m10p.addEventHandler(this, "button3_click31");
  s2m45p = new GButton(this, 1290, 740, 40, 20);
  s2m45p.setText("+45");
  s2m45p.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  s2m45p.addEventHandler(this, "button3_click32");
  abrepinza = new GButton(this, 1090, 740, 70, 30);
  abrepinza.setText("Abrir");
  abrepinza.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  abrepinza.addEventHandler(this, "button3_click33");
  cerrarpinza = new GButton(this, 1090, 780, 70, 30);
  cerrarpinza.setText("Cerrar");
  cerrarpinza.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  cerrarpinza.addEventHandler(this, "button3_click34");
  liberar = new GButton(this, 900, 820, 260, 30);
  liberar.setText("Liberar motores");
  liberar.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  liberar.addEventHandler(this, "button3_click35");
}

// Variable declarations 
// autogenerated do not edit
GButton hablar; 
GTextField textfield1; 
GButton abrir_ojo_iz; 
GButton abrir_ojo_de; 
GButton cerrar_ojo_iz; 
GButton cerrar_ojo_de; 
GButton borrar_historial; 
GButton modo_debug; 
GButton luz_trasera; 
GButton luz_delantera; 
GButton bocina; 
GButton seguir_cara; 
GButton button1; 
GButton button2; 
GSlider gradoscuello; 
GButton CuelloIzq; 
GButton CuelloDer; 
GButton camara1; 
GButton camara2; 
GKnob dirojoder; 
GKnob dirojoizq; 
GButton enviarder; 
GButton enviarizq; 
GButton mhistorial; 
GSlider distojoder; 
GSlider distojoizq; 
GButton selcamara; 
GButton actsensores; 
GButton dactsensores; 
GCheckbox ojoscincro; 
GButton s1a0; 
GButton s1a45; 
GButton s1a90; 
GButton s1a135; 
GButton s1a180; 
GButton s1m1p; 
GButton s1m10p; 
GButton s1m45p; 
GButton s1m1n; 
GButton s1m10n; 
GButton s1m45n; 
GButton s2a0; 
GButton s2a45; 
GButton s2a90; 
GButton s2a135; 
GButton s2a180; 
GButton s2m1n; 
GButton s2m10n; 
GButton s2m45n; 
GButton s2m1p; 
GButton s2m10p; 
GButton s2m45p; 
GButton abrepinza; 
GButton cerrarpinza; 
GButton liberar; 
