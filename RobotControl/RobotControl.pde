// Tested with Processing 3.0a5

// 1) Conectar Arduino.
// 2) Ejecutar script conectar.sh.
// 3) Ejecutar esta aplicación.
// 4) Conectar app MiRobot en android.
// 5) Conectar androidcapture.

import oscP5.*;
import netP5.*;

//import ipcapture.*; // Probando conección a IP Webcam.

import gab.opencv.*;
import java.awt.*;

import processing.serial.*;

import de.bezier.data.sql.*;
import g4p_controls.*;
import com.onlylemi.processing.android.capture.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

//Capture video;
OpenCV opencv;

//IPCapture cam; // Para usar con IP Webcam.

Serial myPort;      // The serial port
int whichKey = -1;  // Variable to hold keystoke values
int inByte = -1;    // Incoming serial data
boolean teclado = true;
boolean luztrasera = false;
boolean luzdelantera = false;
boolean estadobocina = false;
boolean teveocentrado = false;
boolean teveolejos = false;
boolean seguircara = false;
boolean estcam1 = false;
boolean estcam2 = false;
int xcara = 0;
int ycara = 0;

AndroidCamera ac;
PImage img;

String dbHost = "localhost"; // if you are using a using a local database, this should be fine
String dbPort = "3306"; // replace with database port, MAMP standard is 8889
String dbUser = "gabriel"; // replace with database username, MAMP standard is "root"
String dbPass = "12345678";  // replace with database password, MAMP standard is "root"
String dbName = "mirobot"; // replace with database name
String tableName = "instrucciones"; // replace with table name

MySQL msql;

void setup() {
  // Conexión Wifi
  oscP5 = new OscP5(this, 12000);
  myRemoteLocation = new NetAddress("192.168.0.8", 12001);  // IP Teléfono


  // Conexión Bluetooth
  printArray(Serial.list());
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);

  // Conexión por MySQL. Método que ya prácticamente no uso. Es mucho mejor el Osc.
  msql = new MySQL( this, dbHost + ":" + dbPort, dbName, dbUser, dbPass );

  if (msql.connect()) {
    println("Pude conectar a la base de datos.");
  } else {
    println("¡LPQLP, hubo un error!");
  }

  size(1800, 860, JAVA2D);
  createGUI();
  customGUI();

  opencv = new OpenCV(this, 640, 480);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  

//  cam = new IPCapture(this); // Para IP Webcam
//  cam.start("http://192.168.0.6:8080/video", "", "");  // Para IP Teléfono Webcam

  ac = new AndroidCamera(640, 480, 30);
  ac.start();
}
public void draw() {
  //  background(230);
//  if (estcam2) {
//    if (cam.isAvailable()) {  // Para IP Webcam
//      cam.read();    // Para IP Webcam
//      image(cam, width/2, 0);  // Para IP Webcam
//    }  // Para IP Webcam
//  }
  if (estcam1) {
    img = ac.getCameraImage();
    image(img, 0, 0);
    if (seguircara) {
      opencv.loadImage(img);
      noFill();
      stroke(0, 255, 0);
      strokeWeight(3);
      Rectangle[] faces = opencv.detect();

      for (int i = 0; i < faces.length; i++) {
        if ((i==0)&&seguircara) {
          teveocentrado=true;
          teveolejos=false;
          if ((faces[i].x)>(img.width/2)) {
            myPort.write('R');
            teveocentrado=false;
          } else if ((faces[i].x+faces[i].width)<(img.width/2)) {
            myPort.write('L');
            teveocentrado=false;
          } else if (faces[i].y>(img.height/4)) {
            myPort.write('F');
            teveolejos=true;
          }
          if (!teveocentrado) {
            delay(100);
          } else if (teveolejos) {
            delay(200);
          }
        }
        rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
      }
      myPort.write('S');
    }
  }
}

void serialEvent(Serial myPort) {
  inByte = myPort.read();
}

void keyPressed() {
  if (teclado) {
    // Send the keystroke out:
    switch(keyCode) {
    case UP:
      myPort.write('F');
      break;
    case DOWN:
      myPort.write('B');
      break;
    case LEFT:
      myPort.write('L');
      break;
    case RIGHT:
      myPort.write('R');
      break;
    case 107:
      myPort.write('k');
      break;
    case 109:
      myPort.write('o');
    }
    whichKey = key;
  }
}
void keyReleased() {
  if (teclado) {
    myPort.write('S');
  }
}

public void customGUI() {
}

public void MostrarHistorialBD() {
  msql.query("SELECT COUNT(*) FROM " + tableName);
  msql.next();
  //    println("Number of rows: " + msql.getInt(1));
  //    println();

  // access table
  msql.query("SELECT * FROM " + tableName);
  while (msql.next()) {
    // replace "first_name" and "last_name" with column names from your table
    String s1 = msql.getString("Comando");
    String s2 = msql.getString("DatoNum");
    String s3 = msql.getString("DatoTexto");
    println(s1 + "/" + s2 + "/" + s3);
  }
}

public void EnviaraBD(char Comando, int datoNum, String datoTexto) {
  String enviar = "INSERT INTO instrucciones (Comando, DatoNum, DatoTexto) VALUES ('"+Comando+"',"+str(datoNum)+",'"+datoTexto+"')";
  msql.query(enviar);
}

public void EnviarOSC(String Comando, int datoNum, String datoTexto) {
  OscMessage myMessage = new OscMessage("/Comando");
  myMessage.add(Comando);
  myMessage.add(datoNum);
  myMessage.add(datoTexto);  
  oscP5.send(myMessage, myRemoteLocation);
}
void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: "+theOscMessage.addrPattern());
  println(" typetag: "+theOscMessage.typetag());
  switch (theOscMessage.addrPattern()) {
  case "/press":
    println("X: "+theOscMessage.get(0).intValue());
    println("Y: "+theOscMessage.get(1).intValue());
    break;
  case "/Acelerometro":
    println("X: "+theOscMessage.get(0).floatValue());
    println("Y: "+theOscMessage.get(1).floatValue());
    println("Z: "+theOscMessage.get(2).floatValue());
    break;
  case "/Brujula":
    println("X: "+theOscMessage.get(0).floatValue());
    println("Y: "+theOscMessage.get(1).floatValue());
    println("Z: "+theOscMessage.get(2).floatValue());
    break;
  case "/Luz":
    println("V: "+theOscMessage.get(0).floatValue());
    break;
  case "/Distancia":
    println("V: "+theOscMessage.get(0).floatValue());
    break;
  }
}
